package main

import (
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	dirTree(".")

}

func dirTree(directoryName string) {
	file, err := os.Open(directoryName) // For read access.
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	list, _ := file.Readdirnames(0) // 0 to read all files and folders
	for _, name := range list {
		if strings.HasPrefix(name, ".") {
			continue
		}
		fmt.Println(name)

		dirTree(directoryName + string(os.PathSeparator) + name)
	}
}
